const application = require('./dist');
const configOption = require(`./dist/config/config${process.env.NODE_ENV || ''}.json`);
module.exports = application;
var cron = require('node-cron');
var request = require('request');

cron.schedule('* * * * *', () => {
  pushNotification();
});

function pushNotification() {
  var options = {
    'method': 'GET',
    'url': `http://127.0.0.1:3252/pushNotification`,
  };
  return new Promise((resolve, reject) => {
    request(options, function (error, response) {
      if (error) {
        reject(error);
      } else {
        if (response && response.body) {
          let data = JSON.parse(response.body)
          resolve(data);
        }
      }
    });
  })
}
if (require.main === module) {
  // Run the application
  const config = {
    rest: {
      host: configOption.host,
      port: configOption.port,
      openApiSpec: {
        // useful when used with OpenAPI-to-GraphQL to locate your application
        setServersFromRequest: true,
      },
    },
  };
  application.main(config).catch(err => {
    console.error('Cannot start the application.', err);
    process.exit(1);
  });
}

