const configOption = require(`../config/config${process.env.NODE_ENV || ''}.json`);
import * as _ from 'lodash';

export default class Utils {
  static getBaseUrl() {
    return 'http' + (configOption.ssl ? 's' : '') + '://' + configOption.host +
      ((configOption.port === 80 || configOption.port === 443) ? '' : ':' + configOption.port);
  }

  static getClientBaseUrl() {
    return 'http' + (configOption.ssl ? 's' : '') + '://' + configOption.client.host +
      ((configOption.client.port === 80 || configOption.client.port === 443) ? '' : ':' + configOption.client.port);
  }

  static getSiteOptions() {
    return configOption;
  }

  static filterEmailContent(content: any, obj: any) {
    obj = obj || {};
    if (content) {
      let keys = content.match(/\[(.+?)\]/g);
      if (keys && keys.length) {
        keys.forEach(function (k: any) {
          content = content.replace(k, obj[k.slice(1, -1)]);
        });
      }
      return content;
    } else {
      return false;
    }
  }

}
/*
export const siteOptions = configOption;

export function getUrl() {
  return 'http' + (configOption.ssl ? 's' : '') + '://' + configOption.host +
    ((configOption.port === 80 || configOption.port === 443) ? '' : ':' + configOption.port);
}

import * as Utils from '../utils';
console.log(Utils.siteOptions);
        console.log(Utils.getUrl());
*/

