import { Entity, model, property } from '@loopback/repository';

@model()
export class Session extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  userId: string;

  @property({
    type: 'string'
  })
  sessionTime: string;

  @property({
    type: 'number',
    required: true,
  })
  dayStreak: number;

  @property({
    type: 'number',
    default: 0,
  })
  tree: number;

  @property({
    type: 'date',
    default: () => new Date()
  })
  created?: string;

  @property({
    type: 'date',
    default: () => new Date()
  })
  modified?: string;


  constructor(data?: Partial<Session>) {
    super(data);
  }
}

export interface SessionRelations {
  // describe navigational properties here
}

export type SessionWithRelations = Session & SessionRelations;
