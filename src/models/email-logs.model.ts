import { Entity, model, property } from '@loopback/repository';

@model()
export class EmailLogs extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  email: string;

  @property({
    type: 'string',
    required: true,
  })
  status: string;

  @property({
    type: 'date',
    default: () => new Date()
  })
  created?: string;


  constructor(data?: Partial<EmailLogs>) {
    super(data);
  }
}

export interface EmailLogsRelations {
  // describe navigational properties here
}

export type EmailLogsWithRelations = EmailLogs & EmailLogsRelations;
