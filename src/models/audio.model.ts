import { Entity, model, property, belongsTo } from '@loopback/repository';
import { Users, UsersWithRelations } from './users.model'

@model({ settings: {} })
export class Audio extends Entity {
  @property({
    type: 'string',
    required: true,
  })
  name?: string;

  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'number',
  })
  day?: number;

  @property({
    type: 'string',
    required: true,
  })
  bannerImage?: string;

  @property({
    type: 'number',
    default: 1
  })
  status?: number;

  @property({
    type: 'number',
    default: 0
  })
  position?: number;

  @property({
    type: 'string',
  })
  trackImage?: string;

  @property({
    type: 'string',
    required: true,
  })
  type?: string;

  @property({
    type: 'boolean',
    default: false
  })
  isLocked?: boolean;

  @property({
    type: 'array',
    itemType: 'string',
  })
  audioFiles?: string[];

  @property({
    type: 'array',
    itemType: 'string',
    required: false,
  })
  categoryId?: string[];

  @property({
    type: 'array',
    itemType: 'object',
    default: { title: '', audio: '' },
    required: true,
  })
  audio?: object;

  @belongsTo(() => Users, { name: 'user' })
  userId: string

  @property({
    type: 'date',
    default: () => new Date()
  })
  modiefy?: string;

  @property({
    type: 'date',
    default: () => new Date()
  })
  modified?: string;

  @property({
    type: 'date',
    default: () => new Date()
  })
  created?: string;


  constructor(data?: Partial<Audio>) {
    super(data);
  }
}

export interface AudioRelations {
  // describe navigational properties here
}

export type AudioWithRelations = Audio & AudioRelations;
