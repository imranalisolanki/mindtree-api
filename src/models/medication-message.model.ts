import { Entity, model, property } from '@loopback/repository';

@model()
export class MedicationMessage extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  message: string;

  @property({
    type: 'string',
    required: true,
  })
  userId: string;

  @property({
    type: 'date',
    default: () => new Date()
  })
  created?: string;

  @property({
    type: 'date',
    default: () => new Date()
  })
  modified?: string;


  constructor(data?: Partial<MedicationMessage>) {
    super(data);
  }
}

export interface MedicationMessageRelations {
  // describe navigational properties here
}

export type MedicationMessageWithRelations = MedicationMessage & MedicationMessageRelations;
