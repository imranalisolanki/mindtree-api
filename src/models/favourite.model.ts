import {Entity, model, property} from '@loopback/repository';

@model()
export class Favourite extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  userId: string;

  @property({
    type: 'string',
  })
  audioId?: string;

  @property({
    type: 'string',
  })
  dailyAudioId?: string;

  @property({
    type: 'date',
  })
  created?: string;


  constructor(data?: Partial<Favourite>) {
    super(data);
  }
}

export interface FavouriteRelations {
  // describe navigational properties here
}

export type FavouriteWithRelations = Favourite & FavouriteRelations;
