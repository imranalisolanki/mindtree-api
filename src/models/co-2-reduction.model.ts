import { Entity, model, property } from '@loopback/repository';

@model()
export class Co2Reduction extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'number',
    required: true,
  })
  reduction: number;

  @property({
    type: 'number',
  })
  minutes: number;
  @property({
    type: 'number',
    default: 1,
  })
  status: number;

  @property({
    type: 'string',
    required: true,
  })
  userId: string;

  @property({
    type: 'string',
    default: () => new Date()
  })
  created?: string;


  constructor(data?: Partial<Co2Reduction>) {
    super(data);
  }
}

export interface Co2ReductionRelations {
  // describe navigational properties here
}

export type Co2ReductionWithRelations = Co2Reduction & Co2ReductionRelations;
