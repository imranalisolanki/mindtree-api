import { Entity, model, property } from '@loopback/repository';

@model()
export class Daily extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'number',
    required: true,
  })
  day: number;

  @property({
    type: 'string',
    required: true,
  })
  title: string;

  @property({
    type: 'string',
    required: true,
  })
  message: string;

  @property({
    type: 'string',
  })
  bannerImage?: string;

  @property({
    type: 'string',
    required: true,
  })
  audio: string;

  @property({
    type: 'string',
  })
  treeImage?: string;

  @property({
    type: 'string',
  })
  size?: string;

  @property({
    type: 'date',
    default: () => new Date()
  })
  created?: string;

  @property({
    type: 'date',
    default: () => new Date()
  })
  modified?: string;


  constructor(data?: Partial<Daily>) {
    super(data);
  }
}

export interface DailyRelations {
  // describe navigational properties here
}

export type DailyWithRelations = Daily & DailyRelations;
