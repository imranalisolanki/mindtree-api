import { Entity, model, property, AnyObject } from '@loopback/repository';

@model({
  settings: {
    hiddenProperties: ['password']
  }
})
export class Users extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true
  })
  name?: string;

  @property({
    type: 'string',
  })
  address?: string;


  @property({
    type: 'string',
  })
  email?: string;

  @property({
    type: 'string',
  })
  invitedUserId?: string;

  @property({
    type: 'string',
  })
  password?: string;

  @property({
    type: 'string',
    required: false
  })
  gender?: string;

  @property({
    type: 'string',
    required: false
  })
  fcmToken?: string;

  @property({
    type: 'string',
    required: false
  })
  notiTime?: string;

  @property({
    type: 'string',
  })
  about?: string;

  @property({
    type: 'string',
  })
  image?: string;

  @property({
    type: 'number',
    default: 1
  })
  status?: number;

  @property({
    type: 'number',
    default: 1
  })
  treePlanted?: number;

  @property({
    type: 'number',
    default: 1
  })
  co2Reduced?: number;

  @property({
    type: 'date',
  })
  lastAccess?: string;

  @property({
    type: 'object',
    default: { facebookId: '', twitterId: '', googleId: '', appleId: '' },
  })
  social?: AnyObject;

  @property({
    type: 'date',
    default: () => new Date()
  })
  created?: string;

  @property({
    type: 'date',
    default: () => new Date()
  })
  modified?: string;

  constructor(data?: Partial<Users>) {
    super(data);
  }
}

export interface UsersRelations {
  // describe navigational properties here
}

export type UsersWithRelations = Users & UsersRelations;
