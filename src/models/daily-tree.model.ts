import { Entity, model, property } from '@loopback/repository';

@model()
export class DailyTree extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'number',
    required: true,
  })
  day: number;

  @property({
    type: 'string',
    required: false,
  })
  treeImage: string;

  @property({
    type: 'date',
  })
  created?: string;

  @property({
    type: 'date',
  })
  modified?: string;


  constructor(data?: Partial<DailyTree>) {
    super(data);
  }
}

export interface DailyTreeRelations {
  // describe navigational properties here
}

export type DailyTreeWithRelations = DailyTree & DailyTreeRelations;
