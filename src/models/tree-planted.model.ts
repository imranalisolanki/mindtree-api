import { Entity, model, property } from '@loopback/repository';

@model()
export class TreePlanted extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'number',
    required: true,
  })
  planted: number;

  @property({
    type: 'string',
    required: true,
  })
  userId: string;

  @property({
    type: 'date',
    default: () => new Date()
  })
  created?: string;


  constructor(data?: Partial<TreePlanted>) {
    super(data);
  }
}

export interface TreePlantedRelations {
  // describe navigational properties here
}

export type TreePlantedWithRelations = TreePlanted & TreePlantedRelations;
