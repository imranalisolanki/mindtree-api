import { Entity, model, property, belongsTo } from '@loopback/repository';
import { Users, UsersWithRelations } from './users.model'

@model({ settings: {} })
export class Subscription extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @belongsTo(() => Users, { name: 'user' })
  userId: string

  @property({
    type: 'string',
    required: false,
  })
  corporateId: string;

  @property({
    type: 'string',
    default: "active"
  })
  status: string; // active,  inactive

  @property({
    type: 'date',
  })
  expireDate: string;

  @property({
    type: 'date',
    default: () => new Date()
  })
  created?: string;

  constructor(data?: Partial<Subscription>) {
    super(data);
  }
}

export interface SubscriptionRelations {
  user?: UsersWithRelations,
}

export type SubscriptionWithRelations = Subscription & SubscriptionRelations;
