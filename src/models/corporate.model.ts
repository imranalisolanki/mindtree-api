import { Entity, model, property, belongsTo } from '@loopback/repository';
import { Plan, PlanWithRelations } from './plan.model'

@model({ settings: {} })
export class Corporate extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  corporate: string;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
    required: true,
  })
  email: string;

  @property({
    type: 'string',
    required: true,
  })
  mobile: string;

  @property({
    type: 'string',
  })
  address?: string;

  @property({
    type: 'string',
  })
  city?: string;

  @belongsTo(() => Plan, { name: 'plan' })
  planId: string

  @property({
    type: 'number',
    default: 1,
  })
  status?: number;

  @property({
    type: 'string',
    required: true,
  })
  userId: string;

  @property({
    type: 'array',
    itemType: "object"
  })
  employess: object;

  @property({
    type: 'date',
    default: () => new Date()
  })
  created?: string;

  @property({
    type: 'date',
    default: () => new Date()
  })
  modified?: string;


  constructor(data?: Partial<Corporate>) {
    super(data);
  }
}

export interface CorporateRelations {
  plan?: PlanWithRelations,
}

export type CorporateWithRelations = Corporate & CorporateRelations;
