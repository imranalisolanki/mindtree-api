import { mindtreeapiApplication } from './application';
import { ApplicationConfig } from '@loopback/core';

export { mindtreeapiApplication };

export async function main(options: ApplicationConfig = {}) {
  const app = new mindtreeapiApplication(options);
  await app.boot();
  await app.start();
  //app.redirect("https://www.google.com/", 'https://www.google.com/')
  const url = app.restServer.url;
  console.log(`Server is running at ${url}`);
  // console.log(`Try ${url}/ping`);

  return app;
}
