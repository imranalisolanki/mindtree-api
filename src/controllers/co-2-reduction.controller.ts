import { Count, CountSchema, Filter, repository, Where, AnyObject, } from '@loopback/repository';
import { post, param, get, getFilterSchemaFor, getModelSchemaRef, getWhereSchemaFor, patch, put, del, requestBody, HttpErrors, } from '@loopback/rest';
import { Co2Reduction } from '../models';
import { Co2ReductionRepository, UsersRepository } from '../repositories';
import { ObjectID } from 'mongodb';
import moment from 'moment'
import * as _ from 'lodash'
export class Co2ReductionController {
  constructor(
    @repository(Co2ReductionRepository) public co2ReductionRepository: Co2ReductionRepository,
    @repository(UsersRepository) public userRepository: UsersRepository
  ) { }

  @post('/co2reductions', {
    responses: {
      '200': {
        description: 'Co2Reduction model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Co2Reduction) } },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Co2Reduction, {
            title: 'NewCo2Reduction',
            exclude: ['id'],
          }),
        },
      },
    })
    co2Reduction: Omit<Co2Reduction, 'id'>,
  ): Promise<any> {
    let responseData: AnyObject = {}
    responseData = await this.co2ReductionRepository.create(co2Reduction);
    if (co2Reduction && co2Reduction.userId) {
      console.log(co2Reduction.userId)
      const user = await this.userRepository.findOne({
        where: {
          id: co2Reduction.userId
        }
      });
      if (user && user.id) {
        if (!this.co2ReductionRepository.dataSource.connected) {
          await this.co2ReductionRepository.dataSource.connect();
        }
        const TreePlantedCollection = (this.co2ReductionRepository.dataSource.connector as any).collection('Co2Reduction');

        await Promise.all([
          TreePlantedCollection.aggregate([
            {
              $match: {
                userId: new ObjectID(String(user.id)),
              }
            },
            {
              $group: {
                _id: '$userId',
                co2Reduced: { $sum: "$reduction" }
              }
            }
          ]).get()
        ]).then(async (res: any) => {
          console.log(res[0])
          const trees = res && res[0] && res[0][0] && res[0][0].co2Reduced || 0;
          let userObj = {
            co2Reduced: trees
          }
          await this.userRepository.updateById(co2Reduction.userId, userObj);
          return responseData
        }).catch((err: any) => {
          console.debug(err);
        });
      }
    }

  }

  @get('/co2reductions/count', {
    responses: {
      '200': {
        description: 'Co2Reduction model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Co2Reduction)) where?: Where<Co2Reduction>,
  ): Promise<Count> {
    return this.co2ReductionRepository.count(where);
  }

  @get('/co2reductions', {
    responses: {
      '200': {
        description: 'Array of Co2Reduction model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Co2Reduction, { includeRelations: true }),
            },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Co2Reduction)) filter?: Filter<Co2Reduction>,
  ): Promise<Co2Reduction[]> {
    return this.co2ReductionRepository.find(filter);
  }

  @patch('/co2reductions', {
    responses: {
      '200': {
        description: 'Co2Reduction PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Co2Reduction, { partial: true }),
        },
      },
    })
    co2Reduction: Co2Reduction,
    @param.query.object('where', getWhereSchemaFor(Co2Reduction)) where?: Where<Co2Reduction>,
  ): Promise<Count> {
    return this.co2ReductionRepository.updateAll(co2Reduction, where);
  }

  @get('/co2reductions/{id}', {
    responses: {
      '200': {
        description: 'Co2Reduction model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Co2Reduction, { includeRelations: true }),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.query.object('filter', getFilterSchemaFor(Co2Reduction)) filter?: Filter<Co2Reduction>
  ): Promise<Co2Reduction> {
    return this.co2ReductionRepository.findById(id, filter);
  }

  @patch('/co2reductions/{id}', {
    responses: {
      '204': {
        description: 'Co2Reduction PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Co2Reduction, { partial: true }),
        },
      },
    })
    co2Reduction: Co2Reduction,
  ): Promise<void> {
    await this.co2ReductionRepository.updateById(id, co2Reduction);
  }

  @put('/co2reductions/{id}', {
    responses: {
      '204': {
        description: 'Co2Reduction PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() co2Reduction: Co2Reduction,
  ): Promise<void> {
    await this.co2ReductionRepository.replaceById(id, co2Reduction);
  }

  @del('/co2reductions/{id}', {
    responses: {
      '204': {
        description: 'Co2Reduction DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.co2ReductionRepository.deleteById(id);
  }

  @get('/co2reductions/coReductionChart', {
    responses: {
      '200': {
        description: 'Array of co2reductions model instances',
        content: {
          'application/json': {
            schema: { type: 'array' },
          },
        },
      },
    },
  })
  async coReductionChart(
  ): Promise<any> {
    let result: Array<AnyObject> = [];

    var condition = {
      status: 1
    }
    if (!this.co2ReductionRepository.dataSource.connected) {
      await this.co2ReductionRepository.dataSource.connect();
    }
    const treePlantedCollection = (this.co2ReductionRepository.dataSource.connector as any).collection('Co2Reduction');

    return await Promise.all([
      treePlantedCollection.aggregate([
        { $sort: { created: -1 } },
        {
          $match: condition
        },
        {
          $group: {
            _id: {
              year: { $substr: ["$created", 0, 4] },
              month: { $substr: ["$created", 5, 2] },
              day: { $substr: ["$created", 8, 2] },
            },
            count: { $sum: '$reduction' }
          }
        }
      ]).get(),
    ]).then((res: any) => {
      // console.log(res[0], 'res[0]res[0]')
      // return false
      if (res && res[0] && res[0].length) {
        _.forEach(res[0], function (val) {
          result.push([moment(`${val._id.year}-${val._id.month}-${val._id.day}`, 'YYYY-MM-DD:hh:mm:ss').valueOf(), val.count]);
        })
        result = _.sortBy(result, function (v) { return v[0]; }).reverse();
      }
      console.log(result)
      return result;
    }).catch((err: any) => {
      console.log(err)
      throw new HttpErrors.InternalServerError('Something went wrong!')
    });
  }
}
