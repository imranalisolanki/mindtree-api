import { Count, CountSchema, Filter, repository, Where, } from '@loopback/repository';
import { post, param, get, getFilterSchemaFor, getModelSchemaRef, getWhereSchemaFor, patch, put, del, requestBody, } from '@loopback/rest';
import { DailyTree } from '../models';
import { DailyTreeRepository } from '../repositories';

export class DailyTreeController {
  constructor(
    @repository(DailyTreeRepository) public dailyTreeRepository: DailyTreeRepository,
  ) { }

  @post('/daily-trees', {
    responses: {
      '200': {
        description: 'DailyTree model instance',
        content: { 'application/json': { schema: getModelSchemaRef(DailyTree) } },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DailyTree, {
            title: 'NewDailyTree',
            exclude: ['id'],
          }),
        },
      },
    })
    dailyTree: Omit<DailyTree, 'id'>,
  ): Promise<DailyTree> {
    return this.dailyTreeRepository.create(dailyTree);
  }

  @get('/daily-trees/count', {
    responses: {
      '200': {
        description: 'DailyTree model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(DailyTree)) where?: Where<DailyTree>,
  ): Promise<Count> {
    return this.dailyTreeRepository.count(where);
  }

  @get('/daily-trees', {
    responses: {
      '200': {
        description: 'Array of DailyTree model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(DailyTree, { includeRelations: true }),
            },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(DailyTree)) filter?: Filter<DailyTree>,
  ): Promise<DailyTree[]> {
    return this.dailyTreeRepository.find(filter);
  }

  @patch('/daily-trees', {
    responses: {
      '200': {
        description: 'DailyTree PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DailyTree, { partial: true }),
        },
      },
    })
    dailyTree: DailyTree,
    @param.query.object('where', getWhereSchemaFor(DailyTree)) where?: Where<DailyTree>,
  ): Promise<Count> {
    return this.dailyTreeRepository.updateAll(dailyTree, where);
  }

  @get('/daily-trees/{id}', {
    responses: {
      '200': {
        description: 'DailyTree model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(DailyTree, { includeRelations: true }),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.query.object('filter', getFilterSchemaFor(DailyTree)) filter?: Filter<DailyTree>
  ): Promise<DailyTree> {
    return this.dailyTreeRepository.findById(id, filter);
  }

  @patch('/daily-trees/{id}', {
    responses: {
      '204': {
        description: 'DailyTree PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DailyTree, { partial: true }),
        },
      },
    })
    dailyTree: DailyTree,
  ): Promise<void> {
    await this.dailyTreeRepository.updateById(id, dailyTree);
  }

  @put('/daily-trees/{id}', {
    responses: {
      '204': {
        description: 'DailyTree PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() dailyTree: DailyTree,
  ): Promise<void> {
    await this.dailyTreeRepository.replaceById(id, dailyTree);
  }

  @del('/daily-trees/{id}', {
    responses: {
      '204': {
        description: 'DailyTree DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.dailyTreeRepository.deleteById(id);
  }
}
