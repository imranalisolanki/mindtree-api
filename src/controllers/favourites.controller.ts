import { Count, CountSchema, Filter, repository, Where, } from '@loopback/repository';
import { post, param, get, getFilterSchemaFor, getModelSchemaRef, getWhereSchemaFor, patch, put, del, requestBody, } from '@loopback/rest';
import { Favourite } from '../models';
import { FavouriteRepository } from '../repositories';

export class FavouritesController {
  constructor(
    @repository(FavouriteRepository) public favouriteRepository: FavouriteRepository,
  ) { }

  @post('/favourites', {
    responses: {
      '200': {
        description: 'Favourite model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Favourite) } },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Favourite, {
            title: 'NewFavourite',
            exclude: ['id'],
          }),
        },
      },
    })
    favourite: Omit<Favourite, 'id'>,
  ): Promise<Favourite> {
    return this.favouriteRepository.create(favourite);
  }

  @get('/favourites/count', {
    responses: {
      '200': {
        description: 'Favourite model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Favourite)) where?: Where<Favourite>,
  ): Promise<Count> {
    return this.favouriteRepository.count(where);
  }

  @get('/favourites', {
    responses: {
      '200': {
        description: 'Array of Favourite model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Favourite, { includeRelations: true }),
            },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Favourite)) filter?: Filter<Favourite>,
  ): Promise<Favourite[]> {
    return this.favouriteRepository.find(filter);
  }

  @patch('/favourites', {
    responses: {
      '200': {
        description: 'Favourite PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Favourite, { partial: true }),
        },
      },
    })
    favourite: Favourite,
    @param.query.object('where', getWhereSchemaFor(Favourite)) where?: Where<Favourite>,
  ): Promise<Count> {
    return this.favouriteRepository.updateAll(favourite, where);
  }

  @get('/favourites/{id}', {
    responses: {
      '200': {
        description: 'Favourite model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Favourite, { includeRelations: true }),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.query.object('filter', getFilterSchemaFor(Favourite)) filter?: Filter<Favourite>
  ): Promise<Favourite> {
    return this.favouriteRepository.findById(id, filter);
  }

  @patch('/favourites/{id}', {
    responses: {
      '204': {
        description: 'Favourite PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Favourite, { partial: true }),
        },
      },
    })
    favourite: Favourite,
  ): Promise<void> {
    await this.favouriteRepository.updateById(id, favourite);
  }

  @put('/favourites/{id}', {
    responses: {
      '204': {
        description: 'Favourite PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() favourite: Favourite,
  ): Promise<void> {
    await this.favouriteRepository.replaceById(id, favourite);
  }

  @del('/favourites/{id}', {
    responses: {
      '204': {
        description: 'Favourite DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.favouriteRepository.deleteById(id);
  }
}
