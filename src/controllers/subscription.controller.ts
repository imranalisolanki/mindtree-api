import { Count, CountSchema, Filter, repository, Where, } from '@loopback/repository';
import { post, param, get, getFilterSchemaFor, getModelSchemaRef, getWhereSchemaFor, patch, put, del, requestBody, } from '@loopback/rest';
import { Subscription } from '../models';
import { CorporateRepository, SubscriptionRepository, UsersRepository } from '../repositories';
import { OPERATION_SECURITY_SPEC } from '../utils/security-spec';
import { authenticate } from '@loopback/authentication';
import { inject } from '@loopback/core';
import { UserProfile, securityId, SecurityBindings } from '@loopback/security';
export class SubscriptionController {
  constructor(
    @repository(SubscriptionRepository) public subscriptionRepository: SubscriptionRepository,
    @repository(UsersRepository) public usersRepository: UsersRepository,
    @repository(CorporateRepository) public corporateRepository: CorporateRepository

  ) { }

  @post('/subscriptions', {
    responses: {
      '200': {
        description: 'Subscription model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Subscription) } },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Subscription, {
            title: 'NewSubscription',
            exclude: ['id'],
          }),
        },
      },
    })
    subscription: Omit<Subscription, 'id'>,
  ): Promise<Subscription> {
    return this.subscriptionRepository.create(subscription);
  }

  @get('/subscriptions/count', {
    responses: {
      '200': {
        description: 'Subscription model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Subscription)) where?: Where<Subscription>,
  ): Promise<Count> {
    return this.subscriptionRepository.count(where);
  }

  @get('/subscriptions', {
    responses: {
      '200': {
        description: 'Array of Subscription model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Subscription, { includeRelations: true }),
            },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Subscription)) filter?: Filter<Subscription>,
  ): Promise<Subscription[]> {
    return this.subscriptionRepository.find(filter);
  }

  @patch('/subscriptions', {
    responses: {
      '200': {
        description: 'Subscription PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Subscription, { partial: true }),
        },
      },
    })
    subscription: Subscription,
    @param.query.object('where', getWhereSchemaFor(Subscription)) where?: Where<Subscription>,
  ): Promise<Count> {
    return this.subscriptionRepository.updateAll(subscription, where);
  }

  @get('/subscriptions/{id}', {
    responses: {
      '200': {
        description: 'Subscription model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Subscription, { includeRelations: true }),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.query.object('filter', getFilterSchemaFor(Subscription)) filter?: Filter<Subscription>
  ): Promise<Subscription> {
    return this.subscriptionRepository.findById(id, filter);
  }

  @patch('/subscriptions/{id}', {
    responses: {
      '204': {
        description: 'Subscription PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Subscription, { partial: true }),
        },
      },
    })
    subscription: Subscription,
  ): Promise<void> {
    await this.subscriptionRepository.updateById(id, subscription);
  }

  @put('/subscriptions/{id}', {
    responses: {
      '204': {
        description: 'Subscription PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() subscription: Subscription,
  ): Promise<void> {
    await this.subscriptionRepository.replaceById(id, subscription);
  }

  @del('/subscriptions/{id}', {
    responses: {
      '204': {
        description: 'Subscription DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.subscriptionRepository.deleteById(id);
  }

  @get('/subscriptions/getMySubscription', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Subscription  success',
      },
    },
  })
  @authenticate('jwt')
  async getMySubscription(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
  ): Promise<any> {

    const myPlans = await this.subscriptionRepository.findOne({
      where: {
        userId: currentUser[securityId],
        status: "active"
      }
    })
    if (myPlans && myPlans.id) {
      const corporate = await this.corporateRepository.findOne({
        where: {
          id: myPlans && myPlans.corporateId || "null",
          status: 1
        }
      })
      if (corporate && corporate.id) {
        return {}
      } else {
        return myPlans;
      }

    } else {
      return {}
    }
  }
}
