import { Count, CountSchema, Filter, repository, Where, AnyObject, } from '@loopback/repository';
import { post, param, get, getFilterSchemaFor, getModelSchemaRef, getWhereSchemaFor, patch, put, del, requestBody, } from '@loopback/rest';
import { Audio } from '../models';
import { AudioRepository, FavouriteRepository } from '../repositories';
import { OPERATION_SECURITY_SPEC } from '../utils/security-spec';
import { authenticate } from '@loopback/authentication';
import { inject } from '@loopback/core';
import { UserProfile, securityId, SecurityBindings } from '@loopback/security';
import moment from 'moment';
import * as _ from 'lodash'

export class AudioController {
  constructor(
    @repository(AudioRepository) public audioRepository: AudioRepository,
    @repository(FavouriteRepository) public favouriteRepository: FavouriteRepository
  ) { }

  @post('/audio', {
    responses: {
      '200': {
        description: 'Audio model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Audio) } },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Audio, {
            title: 'NewAudio',
            exclude: ['id'],
          }),
        },
      },
    })
    audio: Omit<Audio, 'id'>,
  ): Promise<Audio> {
    return this.audioRepository.create(audio);
  }

  @get('/audio/count', {
    responses: {
      '200': {
        description: 'Audio model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Audio)) where?: Where<Audio>,
  ): Promise<Count> {
    return this.audioRepository.count(where);
  }

  @get('/audio', {
    responses: {
      '200': {
        description: 'Array of Audio model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Audio, { includeRelations: true }),
            },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Audio)) filter?: Filter<Audio>,
  ): Promise<Audio[]> {
    return this.audioRepository.find(filter);
  }

  @patch('/audio', {
    responses: {
      '200': {
        description: 'Audio PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Audio, { partial: true }),
        },
      },
    })
    audio: Audio,
    @param.query.object('where', getWhereSchemaFor(Audio)) where?: Where<Audio>,
  ): Promise<Count> {
    return this.audioRepository.updateAll(audio, where);
  }

  @get('/audio/{id}', {
    responses: {
      '200': {
        description: 'Audio model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Audio, { includeRelations: true }),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.query.object('filter', getFilterSchemaFor(Audio)) filter?: Filter<Audio>
  ): Promise<Audio> {
    return this.audioRepository.findById(id, filter);
  }

  @patch('/audio/{id}', {
    responses: {
      '204': {
        description: 'Audio PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Audio, { partial: true }),
        },
      },
    })
    audio: Audio,
  ): Promise<void> {
    await this.audioRepository.updateById(id, audio);
  }

  @put('/audio/{id}', {
    responses: {
      '204': {
        description: 'Audio PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() audio: Audio,
  ): Promise<void> {
    await this.audioRepository.replaceById(id, audio);
  }

  @del('/audio/{id}', {
    responses: {
      '204': {
        description: 'Audio DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.audioRepository.deleteById(id);
  }

  @get('/audio/getMyAudioList', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Audio  success',
      },
    },
  })
  @authenticate('jwt')
  async getMyAudioList(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
  ): Promise<any> {
    const audioList = await this.audioRepository.find({
      where: {
        userId: currentUser[securityId]
      }, order: ['created DESC'],
      include: [
        { relation: "user" }
      ]
    })
    if (audioList && audioList.length) {
      return audioList;
    } else {
      return []
    }
  }
  @get('/audio/getAudioList', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Audio  success',
      },
    },
  })
  @authenticate('jwt')
  async getAudioList(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
    @param.query.object('data') data: any,
  ): Promise<any> {
    let responseData: Array<AnyObject> = []

    let query: AnyObject = {
      status: { inq: [0, 1] }
    }

    if (data && data.categoryId && data.categoryId.length) {
      query.categoryId = { inq: data.categoryId }
    }

    let audioList = await this.audioRepository.find({
      where: query, order: ['position ASC'],
      include: [
        { relation: "user" }
      ]
    })

    if (audioList && audioList.length) {
      let audioIds = _.map(audioList, v => v.id)
      const favourite = await this.favouriteRepository.find({
        where: {
          userId: currentUser[securityId],
          audioId: { inq: audioIds }
        }
      })

      let favGroup = favourite && favourite.length && _.groupBy(favourite, v => v.audioId)
      let backDate: any = moment(new Date()).add(-7, "days").toISOString()

      let backTimeStamp: any = moment(backDate).unix();

      _.forEach(audioList, function (val: any) {
        let obj = Object.assign({}, val)
        obj.isFavourite = false
        let createDate: any = moment(val.modiefy).unix();
        obj.favourite = favGroup && favGroup[val.id] && favGroup[val.id].length && favGroup[val.id][0] || {}
        if (obj && obj.favourite && obj.favourite.id) {
          obj.isFavourite = true

        }
        if (createDate >= backTimeStamp) {
          obj.isNewAudio = true
        }
        responseData.push(obj)
      })

      return responseData;
    } else {
      return []
    }
  }

  @get('/audio/changeAudioPosition', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Audio  success',
      },
    },
  })
  @authenticate('jwt')
  async changeAudioPosition(
    @param.query.string('position') position: any,
  ): Promise<any> {
    let response: AnyObject = {}
    const positionData = await this.audioRepository.find({
      where: {
        position: { gte: Number(position) }
      }, order: ["position ASC"]
    })
    if (positionData && positionData.length) {
      let _this = this
      _.forEach(positionData, async function (val: any, index) {
        let startPosition = val.position + 1
        if (Number(val.position) == Number(position)) {
          let obj = {
            position: Number(position)
          }
          // await _this.audioRepository.updateAll(val.id, obj)
        } else {
          let obj = {
            position: Number(startPosition)
          }
          await _this.audioRepository.updateAll(val.id, obj)
        }
      })
      response = {
        data: positionData && positionData.length && positionData[0] || {}
      }
      return response
    } else {
      return {}
    }
    // console.log(positionData, '=================-=-=-=-=')
  }
}
