import { Count, CountSchema, Filter, repository, Where, AnyObject, } from '@loopback/repository';
import { post, param, get, getFilterSchemaFor, getModelSchemaRef, getWhereSchemaFor, patch, put, del, requestBody, HttpErrors, } from '@loopback/rest';
import { Corporate } from '../models';
import { UserProfile, securityId, SecurityBindings } from '@loopback/security';
import { CorporateRepository, EmailLogsRepository, SubscriptionRepository, UsersRepository } from '../repositories';
import _ from 'lodash';
import { inject } from '@loopback/core';
import { EmailServiceBindings } from '../keys';
import { EmailService } from '../services/email.service';
import moment from 'moment'
import { OPERATION_SECURITY_SPEC } from '../utils/security-spec';
import { authenticate } from '@loopback/authentication';

export class CorporateController {
  constructor(
    @repository(CorporateRepository) public corporateRepository: CorporateRepository,
    @inject(EmailServiceBindings.MAIL_SERVICE) public emailservice: EmailService,
    @repository(SubscriptionRepository) public subscriptionRepository: SubscriptionRepository,
    @repository(UsersRepository) public usersRepository: UsersRepository,
    @repository(EmailLogsRepository) public emailLogsRepository: EmailLogsRepository
  ) { }

  @post('/corporates', {
    responses: {
      '200': {
        description: 'Corporate model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Corporate) } },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Corporate, {
            title: 'NewCorporate',
            exclude: ['id'],
          }),
        },
      },
    })
    corporate: Omit<Corporate, 'id'>,
  ): Promise<Corporate> {
    return this.corporateRepository.create(corporate);
  }

  @get('/corporates/count', {
    responses: {
      '200': {
        description: 'Corporate model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Corporate)) where?: Where<Corporate>,
  ): Promise<Count> {
    return this.corporateRepository.count(where);
  }

  @get('/corporates', {
    responses: {
      '200': {
        description: 'Array of Corporate model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Corporate, { includeRelations: true }),
            },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Corporate)) filter?: Filter<Corporate>,
  ): Promise<Corporate[]> {
    return this.corporateRepository.find(filter);
  }

  @patch('/corporates', {
    responses: {
      '200': {
        description: 'Corporate PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Corporate, { partial: true }),
        },
      },
    })
    corporate: Corporate,
    @param.query.object('where', getWhereSchemaFor(Corporate)) where?: Where<Corporate>,
  ): Promise<Count> {
    return this.corporateRepository.updateAll(corporate, where);
  }

  @get('/corporates/{id}', {
    responses: {
      '200': {
        description: 'Corporate model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Corporate, { includeRelations: true }),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.query.object('filter', getFilterSchemaFor(Corporate)) filter?: Filter<Corporate>
  ): Promise<any> {
    const corprate = await this.corporateRepository.findOne({
      where: {
        id: id
      }, include: [{ relation: "plan" }]
    })
    return corprate
  }

  @patch('/corporates/{id}', {
    responses: {
      '204': {
        description: 'Corporate PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Corporate, { partial: true }),
        },
      },
    })
    corporate: Corporate,
  ): Promise<void> {
    await this.corporateRepository.updateById(id, corporate);
  }

  @put('/corporates/{id}', {
    responses: {
      '204': {
        description: 'Corporate PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() corporate: Corporate,
  ): Promise<void> {
    await this.corporateRepository.replaceById(id, corporate);
  }

  @del('/corporates/{id}', {
    responses: {
      '204': {
        description: 'Corporate DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.corporateRepository.deleteById(id);
  }

  @post('/corporates/sendEmployeeInvitation', {
    responses: {
      '200': {
        description: 'Email send success',
        content: {
          'application/json': {
            schema: { type: 'object' }
          }
        }
      }
    }
  })
  async sendEmployeeInvitation(@requestBody() data: any): Promise<any> {
    if (data && data.email) {

      let _this = this
      // _.forEach(data.email, async function (val: any) {
      const mailOptions = {
        to: data.email,
        slug: 'user-invitation',
        message: {
          msg: data.msg,
          link: data.link
        }
      };
      const email: any = await _this.emailservice.sendMail(mailOptions)
      let emailObj: AnyObject = {
        email: data.email,
        status: email && email.response || "faild"
      }
      await this.emailLogsRepository.create(emailObj)
      console.log(email && email.response, '===================-=-=-=-')
      return email
      // })
    } else {
      throw new HttpErrors.NotFound("Request data is missing!")
    }
  }
  @post('/corporates/corporateSubscription', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Corporate send success',
        content: {
          'application/json': {
            schema: { type: 'object' }
          }
        }
      }
    }
  })
  @authenticate('jwt')
  async corporateSubscription(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
    @requestBody() data: any): Promise<any> {
    if (data && data.email && data.corporateId) {
      const corporateData = await this.corporateRepository.findOne({
        where: {
          id: data.corporateId
        }, include: [
          { relation: "plan" }
        ]
      })
      if (corporateData && corporateData.id) {
        let corporateEmailData = _.filter(corporateData.employess, function (val: any) {
          return String(val.email) === String(data.email)
        })
        if (corporateEmailData && corporateEmailData.length) {
          let month: any = corporateData && corporateData.plan && corporateData.plan.planType || 0
          let lifetime: any = corporateData && corporateData.plan && corporateData.plan.planType || "null"
          let totalMonths = month.split(' ')[0];
          let subcripPack = month.split(' ')[1];
          let expDate: any = ""
          if (subcripPack === 'years' || subcripPack === 'year') {
            expDate = moment(corporateData.created).add(totalMonths, 'years')
          } else if (subcripPack === 'Months' || subcripPack === 'Month') {
            expDate = moment(corporateData.created).add(totalMonths, 'months')
          } else if (lifetime === 'Lifetime') {
            expDate = moment(corporateData.created).add(50, 'years')
          }

          let subs: AnyObject = {
            userId: currentUser[securityId],
            corporateId: corporateData.id,
            expireDate: expDate
          }
          const subscription = await this.subscriptionRepository.findOne({
            where: {
              userId: currentUser[securityId],
              status: "active"
            }
          })
          if (subscription && subscription.id) {
            throw new HttpErrors.UnprocessableEntity("Subscription is already active!")
          } else {
            return await this.subscriptionRepository.create(subs)
          }
        } else {
          throw new HttpErrors.NotFound("Email not found!")
        }
      } else {
        throw new HttpErrors.NotFound("Corporate not found!")
      }
    } else {
      throw new HttpErrors.NotFound("Request data is missing!")
    }
  }

  @post('/corporates/deleteSubscriptionPlan', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Corporate send success',
        content: {
          'application/json': {
            schema: { type: 'object' }
          }
        }
      }
    }
  })
  @authenticate('jwt')
  async deleteSubscriptionPlan(
    @requestBody() data: any): Promise<any> {
    if (data && data.email) {

      let emp: AnyObject = {
        employess: data.employess
      }
      if (data && data.corporateId) {
        await this.corporateRepository.updateById(data.corporateId, emp)
      }

      const userData = await this.usersRepository.findOne({
        where: {
          email: data.email
        }
      })
      if (userData && userData.id) {

        const subs = await this.subscriptionRepository.findOne({
          where: {
            userId: userData.id,
            corporateId: data.corporateId
          }
        })
        if (subs && subs.id) {

          return await this.subscriptionRepository.deleteById(subs.id)
        }

      } else {
        throw new HttpErrors.NotFound(`User not found from ${data.email}`)
      }

    } else {
      throw new HttpErrors.NotFound("Request data is missing!")
    }
  }
}
