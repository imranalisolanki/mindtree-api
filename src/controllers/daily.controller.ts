import { authenticate } from '@loopback/authentication';
import { inject } from '@loopback/core';
import { AnyObject, Count, CountSchema, Filter, repository, Where, } from '@loopback/repository';
import { post, param, get, getFilterSchemaFor, getModelSchemaRef, getWhereSchemaFor, patch, put, del, requestBody, } from '@loopback/rest';
import { Daily } from '../models';
import { DailyRepository, FavouriteRepository } from '../repositories';
import { OPERATION_SECURITY_SPEC } from '../utils/security-spec';
import { UserProfile, securityId, SecurityBindings } from '@loopback/security';
import * as _ from 'lodash'

export class DailyController {
  constructor(
    @repository(DailyRepository) public dailyRepository: DailyRepository,
    @repository(FavouriteRepository) public favouriteRepository: FavouriteRepository
  ) { }

  @post('/dailies', {
    responses: {
      '200': {
        description: 'Daily model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Daily) } },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Daily, {
            title: 'NewDaily',
            exclude: ['id'],
          }),
        },
      },
    })
    daily: Omit<Daily, 'id'>,
  ): Promise<Daily> {
    return this.dailyRepository.create(daily);
  }

  @get('/dailies/count', {
    responses: {
      '200': {
        description: 'Daily model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Daily)) where?: Where<Daily>,
  ): Promise<Count> {
    return this.dailyRepository.count(where);
  }

  @get('/dailies', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of Daily model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Daily, { includeRelations: true }),
            },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async find(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
    @param.query.object('filter', getFilterSchemaFor(Daily)) filter?: Filter<Daily>,
  ): Promise<any> {
    let responseData: Array<AnyObject> = []
    const dailiesData = await this.dailyRepository.find(filter);
    let dailyIds = _.map(dailiesData, v => v.id)
    const favourite = await this.favouriteRepository.find({
      where: {
        userId: currentUser[securityId],
        dailyAudioId: { inq: dailyIds }
      }
    })
    let favGroup = favourite && favourite.length && _.groupBy(favourite, v => v.dailyAudioId)
    _.forEach(dailiesData, function (val: any) {
      let obj = Object.assign({}, val)
      obj.isFavourite = false
      obj.favourite = favGroup && favGroup[val.id] && favGroup[val.id].length && favGroup[val.id][0] || {}
      if (obj && obj.favourite && obj.favourite.id) {
        obj.isFavourite = true
      }
      responseData.push(obj)
    })
    return responseData
  }

  @patch('/dailies', {
    responses: {
      '200': {
        description: 'Daily PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Daily, { partial: true }),
        },
      },
    })
    daily: Daily,
    @param.query.object('where', getWhereSchemaFor(Daily)) where?: Where<Daily>,
  ): Promise<Count> {
    return this.dailyRepository.updateAll(daily, where);
  }

  @get('/dailies/{id}', {
    responses: {
      '200': {
        description: 'Daily model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Daily, { includeRelations: true }),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.query.object('filter', getFilterSchemaFor(Daily)) filter?: Filter<Daily>
  ): Promise<Daily> {
    return this.dailyRepository.findById(id, filter);
  }

  @patch('/dailies/{id}', {
    responses: {
      '204': {
        description: 'Daily PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Daily, { partial: true }),
        },
      },
    })
    daily: Daily,
  ): Promise<void> {
    await this.dailyRepository.updateById(id, daily);
  }

  @put('/dailies/{id}', {
    responses: {
      '204': {
        description: 'Daily PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() daily: Daily,
  ): Promise<void> {
    await this.dailyRepository.replaceById(id, daily);
  }

  @del('/dailies/{id}', {
    responses: {
      '204': {
        description: 'Daily DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.dailyRepository.deleteById(id);
  }
}
