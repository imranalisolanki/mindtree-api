import { Count, CountSchema, Filter, repository, Where, AnyObject, } from '@loopback/repository';
import { post, param, get, getFilterSchemaFor, getModelSchemaRef, getWhereSchemaFor, patch, put, del, requestBody, HttpErrors, } from '@loopback/rest';
import { Session } from '../models';
import { SessionRepository, UsersRepository } from '../repositories';
import { OPERATION_SECURITY_SPEC } from '../utils/security-spec';
import { authenticate } from '@loopback/authentication';
import { UserProfile, securityId, SecurityBindings } from '@loopback/security';
import { inject } from '@loopback/core';
import moment from 'moment'
import * as _ from 'lodash'
import { ObjectID } from 'mongodb';
export class SessionController {
  constructor(
    @repository(SessionRepository) public sessionRepository: SessionRepository,
    @repository(UsersRepository) public usersRepository: UsersRepository
  ) { }

  @post('/sessions', {
    responses: {
      '200': {
        description: 'Session model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Session) } },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Session, {
            title: 'NewSession',
            exclude: ['id'],
          }),
        },
      },
    })
    session: Omit<Session, 'id'>,
  ): Promise<any> {
    const sessionData = await this.sessionRepository.create(session);
    const user = await this.usersRepository.findOne({
      where: {
        id: session.userId
      }
    })
    if (user && user.id) {
      var now = moment(sessionData.created).format('DD/MM/YYYY HH:mm:ss');
      var then = moment(user.lastAccess).format('DD/MM/YYYY HH:mm:ss');

      let differentTime = moment.utc(moment(now, "DD/MM/YYYY HH:mm").diff(moment(then, "DD/MM/YYYY HH:mm"))).format("HH:mm")
      let obj: any = {
        sessionTime: differentTime
      }
      // session.sessionTime = differentTime
      await this.sessionRepository.updateById(sessionData.id, obj)
    }
    return sessionData
  }

  @get('/sessions/count', {
    responses: {
      '200': {
        description: 'Session model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Session)) where?: Where<Session>,
  ): Promise<Count> {
    return this.sessionRepository.count(where);
  }

  @get('/sessions', {
    responses: {
      '200': {
        description: 'Array of Session model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Session, { includeRelations: true }),
            },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Session)) filter?: Filter<Session>,
  ): Promise<Session[]> {
    return this.sessionRepository.find(filter);
  }

  @patch('/sessions', {
    responses: {
      '200': {
        description: 'Session PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Session, { partial: true }),
        },
      },
    })
    session: Session,
    @param.query.object('where', getWhereSchemaFor(Session)) where?: Where<Session>,
  ): Promise<Count> {
    return this.sessionRepository.updateAll(session, where);
  }

  @get('/sessions/{id}', {
    responses: {
      '200': {
        description: 'Session model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Session, { includeRelations: true }),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.query.object('filter', getFilterSchemaFor(Session)) filter?: Filter<Session>
  ): Promise<Session> {
    return this.sessionRepository.findById(id, filter);
  }

  @patch('/sessions/{id}', {
    responses: {
      '204': {
        description: 'Session PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Session, { partial: true }),
        },
      },
    })
    session: Session,
  ): Promise<void> {
    await this.sessionRepository.updateById(id, session);
  }

  @put('/sessions/{id}', {
    responses: {
      '204': {
        description: 'Session PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() session: Session,
  ): Promise<void> {
    await this.sessionRepository.replaceById(id, session);
  }

  @del('/sessions/{id}', {
    responses: {
      '204': {
        description: 'Session DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.sessionRepository.deleteById(id);
  }

  @get('/sessions/getMySessions', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Session  success',
      },
    },
  })
  @authenticate('jwt')
  async getMySessions(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
  ): Promise<any> {
    const session = await this.sessionRepository.find({
      where: {
        userId: currentUser[securityId]
      }
    })
    if (session && session.length) {
      return session;
    } else {
      return []
    }
  }

  @post('/sessions/getTreeByInvitation', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Session model instance',
      },
    },
  })
  @authenticate('jwt')
  async getTreeByInvitation(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
    @requestBody() data: AnyObject,
  ): Promise<any> {
    if (data && data.userId && data.invitationId) {
      const userData = await this.usersRepository.findOne({
        where: {
          id: data.invitationId,
          invitedUserId: data.userId
        }
      })
      if (userData && userData.id) {
        throw new HttpErrors.NotFound("Already invited!")
      } else {
        const session = await this.sessionRepository.findOne({
          where: {
            userId: data.userId
          }, order: ['created DESC']
        })

        let obj: any = {
          userId: data.userId,
          dayStreak: session && session.dayStreak || 1,
          tree: session && Number(session.tree) + 1 || 1
        }
        let invObj: any = {
          invitedUserId: data.userId
        }
        if (String(data.userId) === String(currentUser[securityId])) {
          throw new HttpErrors.NotFound("Invalid invitation!")
        } else {
          await this.usersRepository.updateById(data.invitationId, invObj)
          return await this.sessionRepository.create(obj)
        }
      }
    } else {
      throw new HttpErrors.NotFound("Request data is missing!")
    }
  }

  @get('/sessions/sessionsGraph', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Session  success',
      },
    },
  })
  @authenticate('jwt')
  async sessionsGraph(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
  ): Promise<any> {
    let date = moment()
    let day = moment(date).startOf('isoWeek').toISOString()
    let end = moment(day).endOf('isoWeek').toISOString()
    let responseDat: Array<AnyObject> = []
    if (!this.sessionRepository.dataSource.connected) {
      await this.sessionRepository.dataSource.connect();
    }
    const treePlantedCollection = (this.sessionRepository.dataSource.connector as any).collection('Session');
    let condition = {
      userId: new ObjectID(currentUser[securityId]),
      created: { '$gt': moment(day).toDate(), '$lt': moment(end).toDate() }
    }
    // console.log(condition, '==============')
    return await Promise.all([
      treePlantedCollection.aggregate([
        {
          $match: condition
        },

      ]).get(),
    ]).then((res: any) => {
      if (res && res[0]) {
        // console.log(res[0])
        let session = res && res[0] && res[0].length && _.groupBy(res[0], v => moment(v.created).format('DD-MM-YYYY'))
        let date = _.map(res[0], v => moment(v.created).format('DD-MM-YYYY')
        )
        // console.log(date, '======**=======')
        let dae = _.uniq(date)
        // console.log(dae, '=============')
        _.forEach(dae, function (val: any) {
          // console.log(session[val], val, '=-=-=-=')
          let obj = session[val] && session[val].length && session[val][0] || {}

          responseDat.push(obj)
        })
        return responseDat;
        return res[0]
      } else {
        return []
      }


    }).catch((err: any) => {
      console.log(err)
      throw new HttpErrors.InternalServerError('Something went wrong!')
    });
  }

}
