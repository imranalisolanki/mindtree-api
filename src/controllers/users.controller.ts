import { Count, CountSchema, Filter, repository, Where, AnyObject, relation, } from '@loopback/repository';
import { post, param, RestApplication, get, getFilterSchemaFor, getModelSchemaRef, getWhereSchemaFor, patch, put, del, requestBody, HttpErrors, RestBindings, Response, } from '@loopback/rest';
import { Users } from '../models';
import { UsersRepository, SubscriptionRepository, SessionRepository, } from '../repositories';
import { CredentialsRequestBody, Credentials } from '../type-schema';
import { inject, Application } from '@loopback/core';
import { UserServiceBindings, TokenServiceBindings, PasswordHasherBindings, EmailServiceBindings, FCMServiceBindings } from '../keys';
import { UserService, authenticate } from '@loopback/authentication';
import { JWTService } from '../services/jwt-service';
import { validateCredentials, validatePassword } from '../services/validator';
import { PasswordHasher } from '../services/hash.password.bcryptjs';
import * as _ from 'lodash'
import { OPERATION_SECURITY_SPEC } from '../utils/security-spec';
import { UserProfile, securityId, SecurityBindings } from '@loopback/security';
import { EmailService } from '../services/email.service';
import moment from 'moment';
import { FCMService } from '../services/fcm.service';
import emoji from 'node-emoji'
// npm i --save-dev @types/node-emoji

export class UsersController {
  constructor(
    @repository(UsersRepository) public usersRepository: UsersRepository,
    @inject(UserServiceBindings.USER_SERVICE) public userService: UserService<Users, Credentials>,
    @inject(TokenServiceBindings.TOKEN_SERVICE) public jwtService: JWTService,
    @inject(EmailServiceBindings.MAIL_SERVICE) public emailservice: EmailService,
    @inject(PasswordHasherBindings.PASSWORD_HASHER) public passwordHasher: PasswordHasher,
    @repository(SubscriptionRepository) public subscriptionRepository: SubscriptionRepository,
    @repository(SessionRepository) public sessionRepository: SessionRepository,
    @inject(RestBindings.Http.RESPONSE) protected response: Response,
    @inject(FCMServiceBindings.FCM_SERVICE) public fcmService: FCMService
  ) { }

  @post('/users', {
    responses: {
      '200': {
        description: 'Users model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Users) } },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Users, {
            title: 'NewUsers',
            exclude: ['id'],
          }),
        },
      },
    })
    users: Omit<Users, 'id'>,
  ): Promise<any> {
    validateCredentials(_.pick(users, ['email', 'password']));
    let responseData: AnyObject = {
      token: ""
    }

    const userExist = await this.usersRepository.findOne({
      where: {
        email: users.email
      }
    })
    if (userExist && userExist.email) {
      throw new HttpErrors.UnprocessableEntity("Email already exist!")
    } else {

      users.password = await this.passwordHasher.hashPassword(users.password || '123456');
      const userDetails = await this.usersRepository.create(users);
      const userProfile = this.userService.convertToUserProfile(userDetails);
      const token = await this.jwtService.generateToken(userProfile);
      let lastLogin = {
        lastAccess: moment().toISOString()
      }
      await this.usersRepository.updateById(userDetails.id, lastLogin)
      responseData = Object.assign({}, userDetails)
      responseData.token = token
      return responseData;
    }

  }

  @get('/users/count', {
    responses: {
      '200': {
        description: 'Users model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Users)) where?: Where<Users>,
  ): Promise<Count> {
    return this.usersRepository.count(where);
  }

  @get('/users', {
    responses: {
      '200': {
        description: 'Array of Users model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Users, { includeRelations: true }),
            },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Users)) filter?: Filter<Users>,
  ): Promise<Users[]> {
    return this.usersRepository.find(filter);
  }

  @patch('/users', {
    responses: {
      '200': {
        description: 'Users PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Users, { partial: true }),
        },
      },
    })
    users: Users,
    @param.query.object('where', getWhereSchemaFor(Users)) where?: Where<Users>,
  ): Promise<Count> {
    return this.usersRepository.updateAll(users, where);
  }

  @post('/users/login', {
    responses: {
      '200': {
        description: 'Token',
        content: {
          'application/json': {
            schema: { type: 'object', properties: { token: { type: 'string' } } }
          }
        }
      }
    }
  })
  async login(@requestBody(CredentialsRequestBody) credentials: Credentials): Promise<any> {
    let responseData: AnyObject = { token: "" }
    const user = await this.userService.verifyCredentials(credentials);
    const userProfile = this.userService.convertToUserProfile(user);
    const token = await this.jwtService.generateToken(userProfile);
    let lastLogin = {
      lastAccess: moment().toISOString()
    }
    await this.usersRepository.updateById(user.id, lastLogin)
    responseData = Object.assign({}, user)
    responseData.token = token
    return responseData;
  }

  @get('/users/{id}', {
    responses: {
      '200': {
        description: 'Users model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Users, { includeRelations: true }),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.query.object('filter', getFilterSchemaFor(Users)) filter?: Filter<Users>
  ): Promise<any> {
    let responseData: AnyObject = {}
    const userDetail = await this.usersRepository.findById(id, filter);
    responseData = Object.assign({}, userDetail)
    const subs = await this.subscriptionRepository.findOne({
      where: {
        userId: id
      }
    })
    responseData.subscription = subs
    return responseData
  }

  @patch('/users/{id}', {
    responses: {
      '204': {
        description: 'Users PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Users, { partial: true }),
        },
      },
    })
    users: Users,
  ): Promise<void> {
    await this.usersRepository.updateById(id, users);
  }

  @put('/users/{id}', {
    responses: {
      '204': {
        description: 'Users PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() users: Users,
  ): Promise<void> {
    await this.usersRepository.replaceById(id, users);
  }

  @del('/users/{id}', {
    responses: {
      '204': {
        description: 'Users DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.usersRepository.deleteById(id);
  }

  @get('/users/getUserList', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Users  success',
      },
    },
  })
  @authenticate('jwt')
  async getUserList(
  ): Promise<any> {
    let responseData: Array<AnyObject> = []

    const users = await this.usersRepository.find({
      where: {
        status: 1
      }
    })
    if (users && users.length) {
      let userIds: Array<any> = _.map(users, v => v.id)
      const subs = await this.subscriptionRepository.find({
        where: {
          userId: { inq: userIds }
        }
      })
      let subscription = subs && subs && subs.length && _.groupBy(subs, v => v.userId)
      _.forEach(users, function (val: any) {
        let obj = Object.assign({}, val)
        obj.subscription = subscription && subscription[val.id] && subscription[val.id][0] || {}
        responseData.push(obj)
      })
      return responseData;
    }
  }

  @post('/users/socialLogin', {
    responses: {
      '200': {
        description: 'Users model instance',
        content: { 'application/json': { schema: { type: 'object' } } },
      },
    },
  })
  async socialLogin(@requestBody() data: any): Promise<any> {
    if (!(data && (data.facebookId || data.twitterId || data.googleId || data.appleId))) {
      throw new HttpErrors.UnprocessableEntity('Social Id is required!');
    }

    let resUser: any;
    let memberObj: any;
    let query: AnyObject = {}
    let userObj: AnyObject = {}
    if (data.facebookId) {
      userObj.social = { facebookId: data.facebookId };
      query = {
        'social.facebookId': data.facebookId
      }
    } else if (data.twitterId) {
      userObj.social = { twitterId: data.twitterId };
      query = {
        'social.twitterId': data.twitterId
      }
    } else if (data.googleId) {
      userObj.social = { googleId: data.googleId };
      query = {
        'social.googleId': data.googleId
      }
    } else if (data.appleId) {
      userObj.social = { appleId: data.appleId };
      query = {
        'social.appleId': data.appleId
      }
    }
    const member = await this.usersRepository.findOne({
      where: query
    });

    if (member && member.id) {
      memberObj = Object.assign({}, member);
    } else {

      userObj.email = data.email;
      userObj.name = data.name;
      userObj.image = data.image;
      if (data && data.email) {
        const userExist = await this.usersRepository.findOne({
          where: {
            email: data.email
          }
        })
        if (userExist && userExist.email) {
          throw new HttpErrors.UnprocessableEntity("Email already exist!")
        }
      }
      memberObj = await this.usersRepository.create(userObj);
    }
    let userProfile: UserProfile = Object.assign(
      { [securityId]: '', name: '', email: '' },
      { [securityId]: memberObj.id, name: memberObj.name, email: memberObj.email }
    );
    let lastLogin = {
      lastAccess: moment().toISOString()
    }

    resUser = Object.assign({}, memberObj);
    resUser.token = await this.jwtService.generateToken(userProfile);
    await this.usersRepository.updateById(resUser.id, lastLogin)
    return resUser;
  }

  @post('/users/sendUserInvitation', {
    responses: {
      '200': {
        description: 'Email send success',
        content: {
          'application/json': {
            schema: { type: 'object' }
          }
        }
      }
    }
  })
  async sendUserInvitation(@requestBody() data: any): Promise<any> {
    if (data && data.email && data.email.length) {
      // console.log(data.email)
      let _this = this
      _.forEach(data.email, async function (val: any) {
        const mailOptions = {
          to: val,
          slug: 'user-invitation',
          message: {
            msg: data.msg,
            link: data.link
          }
        };
        const email = await _this.emailservice.sendMail(mailOptions)
        // console.log(email, '--------------')
        return email
      })
    } else {
      throw new HttpErrors.NotFound("Request data is missing!")
    }
  }

  @get('/users/getInvitedUsers/{userId}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Users GET success',
      },
    },
  })
  @authenticate('jwt')
  async getInvitedUsers(
    @param.path.string('userId') userId: string,
  ): Promise<any> {

    let responseData: Array<AnyObject> = []

    let userInvitationData = await this.usersRepository.find({
      where: {
        invitedUserId: userId
      }, fields: { password: false },
      order: ['created DESC']
    })

    if (userInvitationData && userInvitationData.length) {

      let userIds: Array<any> = _.map(userInvitationData, v => v.id)

      return Promise.all([
        this.sessionRepository.find({
          where: {
            userId: { inq: userIds }
          }
        })
      ]).then(res => {

        let userData = res && res[0] && res[0].length && _.groupBy(res[0], v => v.userId)

        _.forEach(userInvitationData, function (val: any) {

          let obj = Object.assign({}, val)
          let treePlanted = userData && userData[val.id] && userData[val.id].length && userData[val.id] || []

          if (treePlanted && treePlanted.length) {
            obj.tree = _.sumBy(treePlanted, v => v.tree)
          } else {
            obj.tree = 0
          }
          responseData.push(obj)
        })

        return responseData

      }).catch(err => {
        console.log(err)
        throw new HttpErrors.UnprocessableEntity("Something went wrong")
      })
    } else {
      return []
    }
  }

  @post('/users/reset', {
    responses: {
      '200': {
        description: 'Users model instance',
        content: {
          'application/json': {
            schema: { type: 'object', properties: { token: { type: 'string' } } },
          },
        },
      },
    },
  })
  async requestPasswordReset(@requestBody() data: AnyObject): Promise<any> {
    if (!(data && data.email)) {
      throw new HttpErrors.UnprocessableEntity('Email Id is required!');
    }

    const user = await this.usersRepository.findOne({
      where: {
        email: data.email
      },
    });

    if (user && user.id) {
      const userId: string = user.id;
      const otp = Math.floor(100000 + Math.random() * 900000);

      const mailOptions = {
        to: data.email,
        slug: 'verification-otp',
        message: {
          otp: data.otp || otp,
        },
      };

      return Promise.all([
        this.emailservice.sendMail(mailOptions),

      ]).then(async (res: any) => {

        if (res && res[0]) {
          return await this.usersRepository.updateById(userId, user).then(async (rs: any) => {
            let userProfile: UserProfile = Object.assign(
              { [securityId]: '', otp: '', email: '' },
              { [securityId]: user.id, otp: otp, email: user.email, id: user.id }
            );
            const token = await this.jwtService.generateResetToken(userProfile);
            return { token };
          }).catch((er: any) => {
            throw new HttpErrors.UnprocessableEntity(`Error in updating verification otp`);
          });
        }
      }).catch(err => {
        console.log(err, '-----------------')
        throw new HttpErrors.UnprocessableEntity(err)
      })
    } else {
      throw new HttpErrors.NotFound(`User not found for Email ${data.email}`);
    }
  }

  @post('/users/reset-password', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Users model instance',
        content: { 'application/json': { schema: { type: 'object' } } },
      },
    },
  })
  @authenticate('jwt')
  async resetPassword(
    @requestBody() data: AnyObject,
    @inject(SecurityBindings.USER) currentUser: UserProfile,
  ): Promise<void> {
    if (!(data && data.password)) {
      throw new HttpErrors.UnprocessableEntity('Password is required!');
    }
    validatePassword(data.password);

    const user = await this.usersRepository.findOne({
      where: {
        email: currentUser.email
      },
    });

    if (user && user.id) {
      user.password = await this.passwordHasher.hashPassword(data.password);

      return await this.usersRepository
        .updateById(user.id, user)
        .then(async (rs: any) => {
          this.response.status(200);
          return;
        })
        .catch((er: any) => {
          throw new HttpErrors.UnprocessableEntity(
            `Error in updating member password`,
          );
        });
    } else {
      throw new HttpErrors.NotFound(
        `User not found for Email ${currentUser.email}`,
      );
    }
  }

  @get('/pushNotification', {
    responses: {
      '200': {
        description: 'Users model instance',
      },
    },
  })
  async pushNotification(
  ): Promise<void> {

    let pushTime = moment().format('HH:mm')

    const fcmData = await this.usersRepository.find({
      where: {
        status: { inq: [0, 1] }
      }, fields: { name: true, fcmToken: true, notiTime: true, id: true }
    })
    // notiTime
    if (fcmData && fcmData.length) {
      const _this = this
      let emojified = emoji.emojify('Have you Meditated today? :flushed:');
      let bodyMessage = emoji.emojify("Lets reduce your stress, improve your sleep, increase your focus, or simply become more present." + "\n" + "Let's Meditate :blush:");

      _.forEach(fcmData, async function (val: any) {
        if (val && val.notiTime == pushTime) {

          var message = {
            to: val && val.fcmToken,
            data: {
              name: val && val.name || "",
              userId: val && val.id,
              type: 'noti'
            },
            notification: {
              title: emojified,
              body: bodyMessage,
              priority: "high",
              sound: "default",
              vibrate: true,
            }
          };
          await _this.fcmService.sendNotification({ message: message })
        } else {
          console.log(val && val.notiTime, '=====notiTime')
        }
      })
    }
  }

  @post('/users/sendOTP', {
    responses: {
      '200': {
        description: 'Email send success',
        content: {
          'application/json': {
            schema: { type: 'object' }
          }
        }
      }
    }
  })
  async sendOtp(@requestBody() data: any): Promise<any> {
    if (data && data.email && data.otp) {
      let messages: AnyObject = {}
      const mailOptions = {
        to: data.email,
        slug: 'registration-otp',
        message: {
          otp: data.otp
        }
      };

      return Promise.all([
        this.emailservice.sendMail(mailOptions),

      ]).then(res => {
        if (res && res[0]) {
          this.response.status(200);
          messages = {
            message: `Successfully sent otp  to ${data.email}`
          }
          return messages;
        } else {
          throw new HttpErrors.UnprocessableEntity(`Error in sending E-mail to ${data.email}`);
        }
      }).catch(err => {
        throw new HttpErrors.UnprocessableEntity(`Error in sending E-mail to ${data.email}`);

      })

    } else {
      throw new HttpErrors.UnprocessableEntity('Request parameters are missing.');
    }
  }
}
