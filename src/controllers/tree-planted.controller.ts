import { Count, CountSchema, Filter, repository, Where, AnyObject, } from '@loopback/repository';
import { post, param, get, getFilterSchemaFor, getModelSchemaRef, getWhereSchemaFor, patch, put, del, requestBody, HttpErrors, } from '@loopback/rest';
import { TreePlanted } from '../models';
import { TreePlantedRepository, UsersRepository } from '../repositories';
import { ObjectID } from 'mongodb';
import moment from 'moment'
import * as _ from 'lodash'
export class TreePlantedController {
  constructor(
    @repository(TreePlantedRepository) public treePlantedRepository: TreePlantedRepository,
    @repository(UsersRepository) public userRepository: UsersRepository
  ) { }

  @post('/tree-planteds', {
    responses: {
      '200': {
        description: 'TreePlanted model instance',
        content: { 'application/json': { schema: getModelSchemaRef(TreePlanted) } },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TreePlanted, {
            title: 'NewTreePlanted',
            exclude: ['id'],
          }),
        },
      },
    })
    treePlanted: Omit<TreePlanted, 'id'>,
  ): Promise<any> {
    let responseData: AnyObject = {}
    responseData = await this.treePlantedRepository.create(treePlanted);
    if (treePlanted && treePlanted.userId) {
      console.log(treePlanted.userId)
      const user = await this.userRepository.findOne({
        where: {
          id: treePlanted.userId
        }
      });
      if (user && user.id) {
        if (!this.treePlantedRepository.dataSource.connected) {
          await this.treePlantedRepository.dataSource.connect();
        }
        const TreePlantedCollection = (this.treePlantedRepository.dataSource.connector as any).collection('TreePlanted');

        await Promise.all([
          TreePlantedCollection.aggregate([
            {
              $match: {
                userId: new ObjectID(String(user.id)),
              }
            },
            {
              $group: {
                _id: '$userId',
                treePlanted: { $sum: "$planted" }
              }
            }
          ]).get()
        ]).then(async (res: any) => {
          console.log(res[0])
          const trees = res && res[0] && res[0][0] && res[0][0].treePlanted || 0;
          let userObj = {
            treePlanted: trees
          }
          await this.userRepository.updateById(treePlanted.userId, userObj);
          return responseData
        }).catch((err: any) => {
          console.debug(err);
        });
      }
    }
  }

  @get('/tree-planteds/count', {
    responses: {
      '200': {
        description: 'TreePlanted model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(TreePlanted)) where?: Where<TreePlanted>,
  ): Promise<Count> {
    return this.treePlantedRepository.count(where);
  }

  @get('/tree-planteds', {
    responses: {
      '200': {
        description: 'Array of TreePlanted model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(TreePlanted, { includeRelations: true }),
            },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(TreePlanted)) filter?: Filter<TreePlanted>,
  ): Promise<TreePlanted[]> {
    return this.treePlantedRepository.find(filter);
  }

  @patch('/tree-planteds', {
    responses: {
      '200': {
        description: 'TreePlanted PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TreePlanted, { partial: true }),
        },
      },
    })
    treePlanted: TreePlanted,
    @param.query.object('where', getWhereSchemaFor(TreePlanted)) where?: Where<TreePlanted>,
  ): Promise<Count> {
    return this.treePlantedRepository.updateAll(treePlanted, where);
  }

  @get('/tree-planteds/{id}', {
    responses: {
      '200': {
        description: 'TreePlanted model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(TreePlanted, { includeRelations: true }),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.query.object('filter', getFilterSchemaFor(TreePlanted)) filter?: Filter<TreePlanted>
  ): Promise<TreePlanted> {
    return this.treePlantedRepository.findById(id, filter);
  }

  @patch('/tree-planteds/{id}', {
    responses: {
      '204': {
        description: 'TreePlanted PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TreePlanted, { partial: true }),
        },
      },
    })
    treePlanted: TreePlanted,
  ): Promise<void> {
    await this.treePlantedRepository.updateById(id, treePlanted);
  }

  @put('/tree-planteds/{id}', {
    responses: {
      '204': {
        description: 'TreePlanted PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() treePlanted: TreePlanted,
  ): Promise<void> {
    await this.treePlantedRepository.replaceById(id, treePlanted);
  }

  @del('/tree-planteds/{id}', {
    responses: {
      '204': {
        description: 'TreePlanted DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.treePlantedRepository.deleteById(id);
  }

  @get('/tree-planteds/treeChart', {
    responses: {
      '200': {
        description: 'Array of Users model instances',
        content: {
          'application/json': {
            schema: { type: 'array' },
          },
        },
      },
    },
  })
  async treeChart(
    @param.query.string('startDate') startDate?: string,
    @param.query.string('endDate') endDate?: string
  ): Promise<any> {
    let result: Array<AnyObject> = [];

    let query: AnyObject = {
      created: { $lte: moment(endDate).toDate() }
    }

    if (startDate) {
      query.created['$gte'] = moment(startDate).toDate();
    }
    if (endDate) {
      query.created['$lte'] = moment(endDate).toDate();
    }

    if (!this.treePlantedRepository.dataSource.connected) {
      await this.treePlantedRepository.dataSource.connect();
    }
    const treePlantedCollection = (this.treePlantedRepository.dataSource.connector as any).collection('TreePlanted');

    return await Promise.all([
      treePlantedCollection.aggregate([
        { $match: query },
        { $sort: { created: -1 } },
        {
          $group: {
            _id: {
              year: { $substr: ["$created", 0, 4] },
              month: { $substr: ["$created", 5, 2] },
              day: { $substr: ["$created", 8, 2] },
            },
            count: { $sum: '$planted' }
          }
        }
      ]).get(),
    ]).then((res: any) => {

      if (res && res[0] && res[0].length) {
        _.forEach(res[0], function (val) {
          result.push([moment(`${val._id.year}-${val._id.month}-${val._id.day}`, 'YYYY-MM-DD').valueOf(), val.count]);
        })
        result = _.sortBy(result, function (v) { return v[0]; }).reverse();
      }

      return result;
    }).catch((err: any) => {
      throw new HttpErrors.InternalServerError('Something went wrong!')
    });
  }
}
