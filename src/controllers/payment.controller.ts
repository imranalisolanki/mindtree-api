import { Count, CountSchema, Filter, repository, Where, AnyObject, } from '@loopback/repository';
import { post, param, get, getFilterSchemaFor, getModelSchemaRef, getWhereSchemaFor, patch, put, del, requestBody, HttpErrors, } from '@loopback/rest';
import { Payment } from '../models';
import { PaymentRepository, SubscriptionRepository, UsersRepository } from '../repositories';
import { OPERATION_SECURITY_SPEC } from '../utils/security-spec';
import { authenticate } from '@loopback/authentication';
import moment from 'moment'
import * as _ from 'lodash'

export class PaymentController {
  constructor(
    @repository(PaymentRepository) public paymentRepository: PaymentRepository,
    @repository(SubscriptionRepository) public subscriptionRepository: SubscriptionRepository,
    @repository(UsersRepository) public usersRepository: UsersRepository
  ) { }

  @post('/payments', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Payment model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Payment) } },
      },
    },
  })
  @authenticate('jwt')
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Payment, {
            title: 'NewPayment',
            exclude: ['id'],
          }),
        },
      },
    })
    payment: Omit<Payment, 'id'>,
  ): Promise<any> {
    payment.status = "sucess"
    const checkPayment = await this.paymentRepository.findOne({
      where: {
        transactionId: payment && payment.transactionId || "none"
      }
    })
    if (checkPayment && checkPayment.transactionId) {
      throw new HttpErrors.NotFound('Payment already created!')
    }
    const payments = await this.paymentRepository.create(payment);

    let totalMonths = payment.planDuration.split(' ')[0];
    let years = payment.planDuration.split(' ')[1];
    let expDate: any = ""
    if (years === 'years' || years === 'year') {
      expDate = moment(payments.created).add(totalMonths, 'years')
    } else {
      expDate = moment(payments.created).add(totalMonths, 'months')
    }

    // let expDate = moment(payment.transactionDate).add(payment.planDuration, 'months')
    let subs: AnyObject = {
      userId: payment.userId,
      expireDate: expDate
    }
    await this.subscriptionRepository.create(subs)
    return payments
  }

  @get('/payments/count', {
    responses: {
      '200': {
        description: 'Payment model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Payment)) where?: Where<Payment>,
  ): Promise<Count> {
    return this.paymentRepository.count(where);
  }

  @get('/payments', {
    responses: {
      '200': {
        description: 'Array of Payment model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Payment, { includeRelations: true }),
            },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Payment)) filter?: Filter<Payment>,
  ): Promise<Payment[]> {
    return this.paymentRepository.find(filter);
  }

  @patch('/payments', {
    responses: {
      '200': {
        description: 'Payment PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Payment, { partial: true }),
        },
      },
    })
    payment: Payment,
    @param.query.object('where', getWhereSchemaFor(Payment)) where?: Where<Payment>,
  ): Promise<Count> {
    return this.paymentRepository.updateAll(payment, where);
  }

  @get('/payments/{id}', {
    responses: {
      '200': {
        description: 'Payment model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Payment, { includeRelations: true }),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.query.object('filter', getFilterSchemaFor(Payment)) filter?: Filter<Payment>
  ): Promise<Payment> {
    return this.paymentRepository.findById(id, filter);
  }

  @patch('/payments/{id}', {
    responses: {
      '204': {
        description: 'Payment PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Payment, { partial: true }),
        },
      },
    })
    payment: Payment,
  ): Promise<void> {
    await this.paymentRepository.updateById(id, payment);
  }

  @put('/payments/{id}', {
    responses: {
      '204': {
        description: 'Payment PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() payment: Payment,
  ): Promise<void> {
    await this.paymentRepository.replaceById(id, payment);
  }

  @del('/payments/{id}', {
    responses: {
      '204': {
        description: 'Payment DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.paymentRepository.deleteById(id);
  }

  @get('/payments/getPaymentList', {
    responses: {
      '200': {
        description: 'Payment model Sucess'
      },
    },
  })
  async getPaymentList(
  ): Promise<any> {
    const payments: Array<any> = await this.paymentRepository.find({
      where: {
        status: "sucess"
      }
    });
    if (payments && payments.length) {
      let responseData: Array<AnyObject> = []
      let userIds: Array<any> = _.map(payments, v => v.userId)
      return Promise.all([
        this.usersRepository.find({
          where: {
            id: { inq: userIds }
          }, fields: { id: true, name: true, email: true }
        }),
        this.subscriptionRepository.find({
          where: {
            userId: { inq: userIds }
          }, fields: { id: true, expireDate: true, userId: true, created: true, status: true }
        })
      ]).then(res => {

        let usersGroup = res && res[0] && res[0].length && _.groupBy(res[0], v => v.id)
        let subsGroup = res && res[1] && res[1].length && _.groupBy(res[1], v => v.userId)

        _.forEach(payments, function (val: any) {
          let obj = Object.assign({}, val)
          obj.user = usersGroup && usersGroup[val.userId] && usersGroup[val.userId].length && usersGroup[val.userId][0] || {}
          obj.subscription = subsGroup && subsGroup[val.userId] && subsGroup[val.userId].length && subsGroup[val.userId][0] || {}
          responseData.push(obj)
        })
        return responseData

      }).catch(err => {
        throw new HttpErrors.UnprocessableEntity("Something went wrong!")
      })
    } else {
      return []
    }
  }
}
