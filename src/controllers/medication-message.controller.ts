import { Count, CountSchema, Filter, repository, Where, } from '@loopback/repository';
import { post, param, get, getFilterSchemaFor, getModelSchemaRef, getWhereSchemaFor, patch, put, del, requestBody, } from '@loopback/rest';
import { MedicationMessage } from '../models';
import { MedicationMessageRepository } from '../repositories';

export class MedicationMessageController {
  constructor(
    @repository(MedicationMessageRepository) public medicationMessageRepository: MedicationMessageRepository,
  ) { }

  @post('/medication-messages', {
    responses: {
      '200': {
        description: 'MedicationMessage model instance',
        content: { 'application/json': { schema: getModelSchemaRef(MedicationMessage) } },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MedicationMessage, {
            title: 'NewMedicationMessage',
            exclude: ['id'],
          }),
        },
      },
    })
    medicationMessage: Omit<MedicationMessage, 'id'>,
  ): Promise<MedicationMessage> {
    return this.medicationMessageRepository.create(medicationMessage);
  }

  @get('/medication-messages/count', {
    responses: {
      '200': {
        description: 'MedicationMessage model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(MedicationMessage)) where?: Where<MedicationMessage>,
  ): Promise<Count> {
    return this.medicationMessageRepository.count(where);
  }

  @get('/medication-messages', {
    responses: {
      '200': {
        description: 'Array of MedicationMessage model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(MedicationMessage, { includeRelations: true }),
            },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(MedicationMessage)) filter?: Filter<MedicationMessage>,
  ): Promise<MedicationMessage[]> {
    return this.medicationMessageRepository.find(filter);
  }

  @patch('/medication-messages', {
    responses: {
      '200': {
        description: 'MedicationMessage PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MedicationMessage, { partial: true }),
        },
      },
    })
    medicationMessage: MedicationMessage,
    @param.query.object('where', getWhereSchemaFor(MedicationMessage)) where?: Where<MedicationMessage>,
  ): Promise<Count> {
    return this.medicationMessageRepository.updateAll(medicationMessage, where);
  }

  @get('/medication-messages/{id}', {
    responses: {
      '200': {
        description: 'MedicationMessage model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(MedicationMessage, { includeRelations: true }),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.query.object('filter', getFilterSchemaFor(MedicationMessage)) filter?: Filter<MedicationMessage>
  ): Promise<MedicationMessage> {
    return this.medicationMessageRepository.findById(id, filter);
  }

  @patch('/medication-messages/{id}', {
    responses: {
      '204': {
        description: 'MedicationMessage PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MedicationMessage, { partial: true }),
        },
      },
    })
    medicationMessage: MedicationMessage,
  ): Promise<void> {
    await this.medicationMessageRepository.updateById(id, medicationMessage);
  }

  @put('/medication-messages/{id}', {
    responses: {
      '204': {
        description: 'MedicationMessage PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() medicationMessage: MedicationMessage,
  ): Promise<void> {
    await this.medicationMessageRepository.replaceById(id, medicationMessage);
  }

  @del('/medication-messages/{id}', {
    responses: {
      '204': {
        description: 'MedicationMessage DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.medicationMessageRepository.deleteById(id);
  }

  @get('/medication-messages/getUpdatedMessage', {
    responses: {
      '200': {
        description: 'MedicationMessage model instance'
      },
    },
  })
  async getUpdatedMessage(
  ): Promise<any> {
    return this.medicationMessageRepository.findOne({
      order: ['modified DESC'],
      fields: { message: true }
    });
  }

}
