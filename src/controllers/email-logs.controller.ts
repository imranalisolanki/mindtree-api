import { Count, CountSchema, Filter, repository, Where, } from '@loopback/repository';
import { post, param, get, getModelSchemaRef, getFilterSchemaFor, patch, put, del, requestBody, getWhereSchemaFor, getFilterJsonSchemaFor, } from '@loopback/rest';
import { EmailLogs } from '../models';
import { EmailLogsRepository } from '../repositories';

export class EmailLogsController {
  constructor(
    @repository(EmailLogsRepository)
    public emailLogsRepository: EmailLogsRepository,
  ) { }

  @post('/email-logs', {
    responses: {
      '200': {
        description: 'EmailLogs model instance',
        content: { 'application/json': { schema: getModelSchemaRef(EmailLogs) } },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(EmailLogs, {
            title: 'NewEmailLogs',
            exclude: ['id'],
          }),
        },
      },
    })
    emailLogs: Omit<EmailLogs, 'id'>,
  ): Promise<EmailLogs> {
    return this.emailLogsRepository.create(emailLogs);
  }

  @get('/email-logs/count', {
    responses: {
      '200': {
        description: 'EmailLogs model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(EmailLogs)) where?: Where<EmailLogs>,
  ): Promise<Count> {
    return this.emailLogsRepository.count(where);
  }

  @get('/email-logs', {
    responses: {
      '200': {
        description: 'Array of EmailLogs model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(EmailLogs, { includeRelations: true }),
            },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(EmailLogs)) filter?: Filter<EmailLogs>
  ): Promise<EmailLogs[]> {
    return this.emailLogsRepository.find(filter);
  }

  // @patch('/email-logs', {
  //   responses: {
  //     '200': {
  //       description: 'EmailLogs PATCH success count',
  //       content: { 'application/json': { schema: CountSchema } },
  //     },
  //   },
  // })
  // async updateAll(
  //   @requestBody({
  //     content: {
  //       'application/json': {
  //         schema: getModelSchemaRef(EmailLogs, { partial: true }),
  //       },
  //     },
  //   })
  //   emailLogs: EmailLogs,
  //   @param.where(EmailLogs) where?: Where<EmailLogs>,
  // ): Promise<Count> {
  //   return this.emailLogsRepository.updateAll(emailLogs, where);
  // }

  // @get('/email-logs/{id}', {
  //   responses: {
  //     '200': {
  //       description: 'EmailLogs model instance',
  //       content: {
  //         'application/json': {
  //           schema: getModelSchemaRef(EmailLogs, { includeRelations: true }),
  //         },
  //       },
  //     },
  //   },
  // })
  // async findById(
  //   @param.path.string('id') id: string,
  //   @param.filter(EmailLogs, { exclude: 'where' }) filter?: FilterExcludingWhere<EmailLogs>
  // ): Promise<EmailLogs> {
  //   return this.emailLogsRepository.findById(id, filter);
  // }

  // @patch('/email-logs/{id}', {
  //   responses: {
  //     '204': {
  //       description: 'EmailLogs PATCH success',
  //     },
  //   },
  // })
  // async updateById(
  //   @param.path.string('id') id: string,
  //   @requestBody({
  //     content: {
  //       'application/json': {
  //         schema: getModelSchemaRef(EmailLogs, { partial: true }),
  //       },
  //     },
  //   })
  //   emailLogs: EmailLogs,
  // ): Promise<void> {
  //   await this.emailLogsRepository.updateById(id, emailLogs);
  // }

  @put('/email-logs/{id}', {
    responses: {
      '204': {
        description: 'EmailLogs PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() emailLogs: EmailLogs,
  ): Promise<void> {
    await this.emailLogsRepository.replaceById(id, emailLogs);
  }

  @del('/email-logs/{id}', {
    responses: {
      '204': {
        description: 'EmailLogs DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.emailLogsRepository.deleteById(id);
  }
}
