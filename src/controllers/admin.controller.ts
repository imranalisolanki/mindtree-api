import { Count, CountSchema, Filter, repository, Where, } from '@loopback/repository';
import { post, param, get, getFilterSchemaFor, getModelSchemaRef, getWhereSchemaFor, patch, put, del, requestBody, HttpErrors, } from '@loopback/rest';
import { Admin } from '../models';
import { AdminRepository } from '../repositories';
import { securityId, SecurityBindings, UserProfile } from '@loopback/security';
import { validateCredentials, validatePassword } from '../services/validator';
import { inject } from '@loopback/core';
import { PasswordHasherBindings, UserServiceBindings, TokenServiceBindings } from '../keys';
import { PasswordHasher } from '../services/hash.password.bcryptjs';
import * as _ from 'lodash'
import { CredentialsRequestBody, Credentials } from '../type-schema';
import { UserService } from '@loopback/authentication';
import { JWTService } from '../services/jwt-service';
export class AdminController {
  constructor(
    @repository(AdminRepository) public adminRepository: AdminRepository,
    @inject(PasswordHasherBindings.PASSWORD_HASHER) public passwordHasher: PasswordHasher,
    @inject(UserServiceBindings.USER_SERVICE) public userService: UserService<Admin, Credentials>,
    @inject(TokenServiceBindings.TOKEN_SERVICE) public jwtService: JWTService
  ) { }

  @post('/admins', {
    responses: {
      '200': {
        description: 'Admin model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Admin) } },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Admin, {
            title: 'NewAdmin',
            exclude: ['id'],
          }),
        },
      },
    })
    admin: Omit<Admin, 'id'>,
  ): Promise<Admin> {
    validateCredentials(_.pick(admin, ['email', 'password']));

    admin.password = await this.passwordHasher.hashPassword(admin.password || '123456');

    return this.adminRepository.create(admin);
  }
  @post('/admins/login', {
    responses: {
      '200': {
        description: 'Token',
        content: {
          'application/json': {
            schema: { type: 'object', properties: { token: { type: 'string' } } }
          }
        }
      }
    }
  })
  async login(@requestBody(CredentialsRequestBody) credentials: Credentials): Promise<{ token: string }> {
    const invalidCredentialsError = 'Invalid email or password.';

    const admin = await this.adminRepository.findOne({
      where: {
        email: credentials.email
      }
    });
    if (!admin) {
      throw new HttpErrors.Unauthorized(invalidCredentialsError);
    }
    const passwordMatched = await this.passwordHasher.comparePassword(credentials.password, admin.password || '');

    if (!passwordMatched) {
      throw new HttpErrors.Unauthorized(invalidCredentialsError);
    }
    const userProfile = {
      [securityId]: admin.id || '',
      name: admin.name || '',
      email: admin.email || '',
    }
    const token = await this.jwtService.generateToken(userProfile);

    return { token };
  }

  @get('/admins/count', {
    responses: {
      '200': {
        description: 'Admin model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Admin)) where?: Where<Admin>,
  ): Promise<Count> {
    return this.adminRepository.count(where);
  }

  @get('/admins', {
    responses: {
      '200': {
        description: 'Array of Admin model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Admin, { includeRelations: true }),
            },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Admin)) filter?: Filter<Admin>,
  ): Promise<Admin[]> {
    return this.adminRepository.find(filter);
  }

  @patch('/admins', {
    responses: {
      '200': {
        description: 'Admin PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Admin, { partial: true }),
        },
      },
    })
    admin: Admin,
    @param.query.object('where', getWhereSchemaFor(Admin)) where?: Where<Admin>,
  ): Promise<Count> {
    return this.adminRepository.updateAll(admin, where);
  }

  @get('/admins/{id}', {
    responses: {
      '200': {
        description: 'Admin model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Admin, { includeRelations: true }),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.query.object('filter', getFilterSchemaFor(Admin)) filter?: Filter<Admin>
  ): Promise<Admin> {
    return this.adminRepository.findById(id, filter);
  }

  @patch('/admins/{id}', {
    responses: {
      '204': {
        description: 'Admin PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Admin, { partial: true }),
        },
      },
    })
    admin: Admin,
  ): Promise<void> {
    await this.adminRepository.updateById(id, admin);
  }

  @put('/admins/{id}', {
    responses: {
      '204': {
        description: 'Admin PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() admin: Admin,
  ): Promise<void> {
    await this.adminRepository.replaceById(id, admin);
  }

  @del('/admins/{id}', {
    responses: {
      '204': {
        description: 'Admin DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.adminRepository.deleteById(id);
  }
}
