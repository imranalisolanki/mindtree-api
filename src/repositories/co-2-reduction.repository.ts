import {DefaultCrudRepository} from '@loopback/repository';
import {Co2Reduction, Co2ReductionRelations} from '../models';
import {DbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class Co2ReductionRepository extends DefaultCrudRepository<
  Co2Reduction,
  typeof Co2Reduction.prototype.id,
  Co2ReductionRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(Co2Reduction, dataSource);
  }
}
