import { DefaultCrudRepository, BelongsToAccessor, repository } from '@loopback/repository';
import { Subscription, SubscriptionRelations, Users, Plan } from '../models';
import { DbDataSource } from '../datasources';
import { inject, Getter } from '@loopback/core';
import { UsersRepository } from './users.repository';
// import { PlanRepository } from './plan.repository';

export class SubscriptionRepository extends DefaultCrudRepository<Subscription, typeof Subscription.prototype.id, SubscriptionRelations> {
  public readonly user: BelongsToAccessor<Users, typeof Users.prototype.id>;
  // public readonly plan: BelongsToAccessor<Plan, typeof Plan.prototype.id>;
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
    @repository.getter('UsersRepository') public userRepositoryGitter: Getter<UsersRepository>,
    // @repository.getter('PlanRepository') public planRepositoryGitter: Getter<PlanRepository>,
  ) {
    super(Subscription, dataSource);

    this.user = this.createBelongsToAccessorFor('user', userRepositoryGitter);
    // this.plan = this.createBelongsToAccessorFor('plan', planRepositoryGitter);

    this.registerInclusionResolver('user', this.user.inclusionResolver);
    // this.registerInclusionResolver('plan', this.plan.inclusionResolver);
  }
}
