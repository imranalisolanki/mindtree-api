import {DefaultCrudRepository} from '@loopback/repository';
import {EmailLogs, EmailLogsRelations} from '../models';
import {DbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class EmailLogsRepository extends DefaultCrudRepository<
  EmailLogs,
  typeof EmailLogs.prototype.id,
  EmailLogsRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(EmailLogs, dataSource);
  }
}
