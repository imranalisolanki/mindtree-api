import {DefaultCrudRepository} from '@loopback/repository';
import {DailyTree, DailyTreeRelations} from '../models';
import {DbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class DailyTreeRepository extends DefaultCrudRepository<
  DailyTree,
  typeof DailyTree.prototype.id,
  DailyTreeRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(DailyTree, dataSource);
  }
}
