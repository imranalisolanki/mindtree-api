import { DefaultCrudRepository } from '@loopback/repository';
import { MedicationMessage, MedicationMessageRelations } from '../models';
import { DbDataSource } from '../datasources';
import { inject } from '@loopback/core';

export class MedicationMessageRepository extends DefaultCrudRepository<
  MedicationMessage,
  typeof MedicationMessage.prototype.id,
  MedicationMessageRelations
  > {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(MedicationMessage, dataSource);

    (this.modelClass as any).observe('persist', async (ctx: any) => {
      ctx.data.modified = new Date();
    });

  }
}
