import { DefaultCrudRepository, BelongsToAccessor, repository } from '@loopback/repository';
import { Corporate, CorporateRelations, Plan } from '../models';
import { DbDataSource } from '../datasources';
import { inject, Getter } from '@loopback/core';
import { PlanRepository } from './plan.repository';

export class CorporateRepository extends DefaultCrudRepository<Corporate, typeof Corporate.prototype.id, CorporateRelations> {
  public readonly plan: BelongsToAccessor<Plan, typeof Plan.prototype.id>;
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
    @repository.getter('PlanRepository') public planRepositoryGitter: Getter<PlanRepository>,
  ) {
    super(Corporate, dataSource);

    this.plan = this.createBelongsToAccessorFor('plan', planRepositoryGitter);

    this.registerInclusionResolver('plan', this.plan.inclusionResolver);
  }
}
