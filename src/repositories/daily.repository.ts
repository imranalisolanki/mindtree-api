import {DefaultCrudRepository} from '@loopback/repository';
import {Daily, DailyRelations} from '../models';
import {DbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class DailyRepository extends DefaultCrudRepository<
  Daily,
  typeof Daily.prototype.id,
  DailyRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(Daily, dataSource);
  }
}
