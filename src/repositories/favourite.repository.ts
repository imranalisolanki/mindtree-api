import {DefaultCrudRepository} from '@loopback/repository';
import {Favourite, FavouriteRelations} from '../models';
import {DbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class FavouriteRepository extends DefaultCrudRepository<
  Favourite,
  typeof Favourite.prototype.id,
  FavouriteRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(Favourite, dataSource);
  }
}
