import { DefaultCrudRepository, BelongsToAccessor, repository } from '@loopback/repository';
import { Audio, AudioRelations, Users } from '../models';
import { DbDataSource } from '../datasources';
import { inject, Getter } from '@loopback/core';
import { UsersRepository } from './users.repository';

export class AudioRepository extends DefaultCrudRepository<Audio, typeof Audio.prototype.id, AudioRelations> {
  public readonly user: BelongsToAccessor<Users, typeof Users.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
    @repository.getter('UsersRepository') public userRepositoryGitter: Getter<UsersRepository>,

  ) {
    super(Audio, dataSource);
    this.user = this.createBelongsToAccessorFor('user', userRepositoryGitter);

    this.registerInclusionResolver('user', this.user.inclusionResolver);

    (this.modelClass as any).observe('persist', async (ctx: any) => {
      ctx.data.modiefy = new Date();
    });

  }
}
