import {DefaultCrudRepository} from '@loopback/repository';
import {TreePlanted, TreePlantedRelations} from '../models';
import {DbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class TreePlantedRepository extends DefaultCrudRepository<
  TreePlanted,
  typeof TreePlanted.prototype.id,
  TreePlantedRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(TreePlanted, dataSource);
  }
}
