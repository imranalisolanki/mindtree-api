import { HttpErrors } from '@loopback/rest';
import { Users } from '../models/users.model'
import { UserService } from '@loopback/authentication';
import { UserProfile, securityId } from '@loopback/security';
import { repository } from '@loopback/repository';
import { PasswordHasher } from './hash.password.bcryptjs';
import { PasswordHasherBindings } from '../keys';
import { inject } from '@loopback/context';
import { Credentials } from '../type-schema';
import { UsersRepository } from '../repositories';
import * as _ from 'lodash';

export class UsersService implements UserService<Users, Credentials> {
  constructor(
    @repository(UsersRepository) public UsersRepository: UsersRepository,
    @inject(PasswordHasherBindings.PASSWORD_HASHER) public passwordHasher: PasswordHasher,
  ) { }

  async verifyCredentials(credentials: Credentials): Promise<Users> {
    const invalidCredentialsError = 'Invalid email or password.';

    const getUsers = await this.UsersRepository.findOne({ where: { email: credentials.email }, });

    if (!getUsers) {
      throw new HttpErrors.Unauthorized(invalidCredentialsError);
    }

    const passwordMatched = await this.passwordHasher.comparePassword(credentials.password, getUsers.password || '');

    if (!passwordMatched) {
      throw new HttpErrors.Unauthorized(invalidCredentialsError);
    }

    return getUsers;
  }

  convertToUserProfile(user: Users): UserProfile {
    return {
      [securityId]: user.id || '',
      lname: user.name || '',
      email: user.email || '',
    };
  }
}
