const siteOption = require(`../config/config${process.env.NODE_ENV || ''}.json`);
import * as _ from 'lodash';

export function getSiteOptions() {
  return siteOption;
}

export function filterEmailContent(content: any, obj: any) {
  obj = obj || {};
  if (content) {
    let keys = content.match(/\[(.+?)\]/g);
    if (keys && keys.length) {
      keys.forEach(function (k: any) {
        content = content.replace(k, obj[k.slice(1, -1)]);
      });
    }
    return content;
  } else return false;
}
export function calculateStripeFee(amount: number) {
  var sFees = (amount + 0.3) / ((100 - 2.9) / 100);
  return { total: sFees, fee: sFees - amount }
}

