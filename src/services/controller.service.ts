import { bind, BindingScope, inject } from '@loopback/core';
import { repository, AnyObject } from '@loopback/repository';
import { UsersRepository } from '../repositories';
import * as _ from 'lodash';
import { HttpErrors } from '@loopback/rest';

@bind({ scope: BindingScope.TRANSIENT })
export class ControllerService {
  constructor(
    @repository(UsersRepository) public usersRepository: UsersRepository,
  ) { }

  /* async refreshAverageRating(data: AnyObject): Promise<any> {
    if (data && data.userId) {

      const user = await this.usersRepository.findOne({
        where: {
          id: data.userId,
          status: 1
        }
      });

      if (user && user.id) {
        if (!this.reviewRepository.dataSource.connected) {
          await this.reviewRepository.dataSource.connect();
        }
        const reviewCollection = (this.reviewRepository.dataSource.connector as any).collection('Review');

        await Promise.all([
          reviewCollection.aggregate([
            {
              $match: {
                userId: data.userId,
              }
            },
            {
              $group: {
                _id: '$userId',
                avgRating: {
                  $avg: "$rating"
                }
              }
            }
          ]).get()
        ]).then(async (res: any) => {

          const rating = res && res[0] && res[0][0] && res[0][0].avgRating || 0;
          let userObj = {
            rating: rating
          }
          await this.usersRepository.updateById(data.userId, userObj);
        }).catch((err: any) => {
          console.debug(err);
        });
      }
    }
  } */

}
