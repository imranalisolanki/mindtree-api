import { Credentials } from '../type-schema';
import * as isemail from 'isemail';
import { HttpErrors } from '@loopback/rest';

export function validateCredentials(credentials: Credentials | any) {
  if (!isemail.validate(credentials.email)) {
    throw new HttpErrors.UnprocessableEntity('invalid email');
  }

  if (!credentials.password || credentials.password.length < 6) {
    throw new HttpErrors.UnprocessableEntity('password must be minimum 6 characters');
  }
}

export function validatePassword(password: String) {
  if (password.length < 6) {
    throw new HttpErrors.UnprocessableEntity('password must be minimum 6 characters');
  }
}
