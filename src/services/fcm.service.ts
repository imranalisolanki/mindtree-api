import { bind, BindingScope } from '@loopback/core';
import { HttpErrors } from '@loopback/rest';
import * as common from '../services/common';
import { AnyObject, repository } from '@loopback/repository';
const FCM = require('fcm-push');
import * as _ from 'lodash'
import fetch from 'node-fetch';
// npm i --save-dev @types/node-fetch
@bind({ scope: BindingScope.TRANSIENT })
export class FCMService {
  constructor(
  ) { }

  siteOption = common.getSiteOptions();
  fcm = new FCM(this.siteOption.fcm.secretKey);

  async sendNotification(data: any): Promise<any> {

    if (!(data && data.message)) {
      throw new HttpErrors['NotFound'](`FCM Token not found.`);
    }

    await this.fcm.send(data.message)
      .then((response: any) => {
        console.log("Successfully sent with response: ", response);
      })
      .catch((err: any) => {
        console.log("Something has gone wrong!");
        console.log(err);
      })


  }

}
